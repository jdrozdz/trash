#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#define ON	1
#define OFF	0

//-- led+buzzer
#define ALARM_DIR		DDRC
#define ALARM_PORT		PORTC
#define ALARM_PIN		3
#define ALARM_ENABLE	ALARM_DIR |= (ON << ALARM_PIN )
#define ALARM_ON		ALARM_PORT |= (ON << ALARM_PIN)
#define ALARM_OFF		ALARM_PORT = (OFF << ALARM_PIN)
//-- switch off alarm
#define SW_ALARM_DIR	DDRD
#define SW_ALARM_PORT	PORTD
#define SW_ALARM_PIN	2
#define SW_ALARM_ENABLE	SW_ALARM_DIR &= ~(ON << SW_ALARM_PIN)
#define SW_ALARM_ON		SW_ALARM_PORT |= (ON << SW_ALARM_PIN)
#define SW_ALARM_STATUS SW_ALARM_PORT & (ON << SW_ALARM_PIN)
//-- pir sensor
#define SENSOR_DIR		DDRD
#define SENSOR_PORT		PORTD
#define SENSOR_PIN		4
#define SENSOR_ENABLE	SENSOR_DIR &= ~(ON << SENSOR_PIN)
#define SENSOR_ON		SENSOR_PORT |= (ON << SENSOR_PIN)
#define SENSOR_STATUS	SENSOR_PORT & (ON << SENSOR_PIN)


int main(void) {
	
	ALARM_ENABLE; ALARM_ON;
	SW_ALARM_ENABLE; SW_ALARM_ON;
	SENSOR_ENABLE; SENSOR_ON;
	
	GICR |=  (1 << INT0);
	MCUCR |= (1 << ISC00);
	
	sei();
    while(1) {
        if(!SENSOR_STATUS){
			ALARM_ON;
		}
    }
}

ISR(INT0_vect) {
	if(!SW_ALARM_STATUS) {
		_delay_ms(10);
		if(!SW_ALARM_STATUS) {
			ALARM_OFF;
		}
	}
}